# Email Notification / SMTP settings (in progress)

1. bash to gitlab docker container

1. Edit `/etc/gitlab/gitlab.rb`

Remove # to enable configuration

gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = 'localhost'
gitlab_rails['smtp_port'] = 25
gitlab_rails['smtp_domain'] = 'localhost'
gitlab_rails['smtp_tls'] = false
gitlab_rails['smtp_openssl_verify_mode'] = 'none'
gitlab_rails['smtp_enable_starttls_auto'] = false
gitlab_rails['smtp_ssl'] = false
gitlab_rails['smtp_force_ssl'] = false

remove the settings for smtp_user_name and smtp_password.

# If your SMTP server does not like the default 'From: gitlab@localhost' you

# can change the 'From' with this setting.

gitlab_rails['gitlab_email_from'] = 'gitlab@example.com'
gitlab_rails['gitlab_email_reply_to'] = 'noreply@example.com'

# If your SMTP server is using a self signed certificate or a certificate which

# is signed by a CA which is not trusted by default, you can specify a custom ca file.

# Please note that the certificates from /etc/gitlab/trusted-certs/ are

# not used for the verification of the SMTP server certificate.

gitlab_rails['smtp_ca_file'] = '/path/to/your/cacert.pem'

[console to gitlab_rails](/readme/readme_rails_console.md)

```bash
docker exec -it <container-id> gitlab-rails console
```

## Testing notify

```bash
Notify.test_email('kitsupdo@gmail.com', 'Message Subject', 'Message Body').deliver_now
```

references:<https://docs.gitlab.com/omnibus/settings/smtp.html>
